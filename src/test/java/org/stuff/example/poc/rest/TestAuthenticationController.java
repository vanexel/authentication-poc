package org.stuff.example.poc.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.stuff.example.poc.util.JsonHelper.toJson;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.stuff.example.poc.model.dto.LoginDTO;

@SpringBootTest
@AutoConfigureMockMvc
public class TestAuthenticationController {

    private final MockMvc mockMvc;
    private final ObjectMapper objectMapper;

    private final String password = "test";
    private final String wrongPassword = "anotherpassword";
    private final String username = "test1@test.com";

    @Autowired
    public TestAuthenticationController(MockMvc mockMvc, ObjectMapper objectMapper) {
        this.mockMvc = mockMvc;
        this.objectMapper = objectMapper;
    }

    @Test
    public void testLoginSuccess() throws Exception {
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setUserName(username);
        loginDTO.setPassword(password);

        MvcResult createResult = this.mockMvc
                .perform(post("/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(objectMapper, loginDTO)))
                .andExpect(status().isOk())
                .andExpect(header().exists(HttpHeaders.AUTHORIZATION))
                .andReturn();

    }

    @Test
    public void testLoginFail() throws Exception {
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setUserName(username);
        loginDTO.setPassword(wrongPassword);

        this.mockMvc
                .perform(post("/auth/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJson(objectMapper, loginDTO)))
                .andExpect(status().isUnauthorized())
                .andExpect(header().doesNotExist(HttpHeaders.AUTHORIZATION))
                .andReturn();
    }
}
