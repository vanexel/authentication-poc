package org.stuff.example.poc.rest;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest
@AutoConfigureMockMvc
public class TestUserController {
    private final MockMvc mockMvc;
    private final ObjectMapper objectMapper;

    private final String username = "test1@test.com";
    private final String wrongUsername = "testu@test.com";
    @Value("${jwt.testAuthorization}")
    private String authorization;

    @Autowired
    public TestUserController(MockMvc mockMvc, ObjectMapper objectMapper) {
        this.mockMvc = mockMvc;
        this.objectMapper = objectMapper;
    }

    @Test
    public void testGetUserSuccess() throws Exception {
        MvcResult getResult = this.mockMvc
                .perform(get(String.format("/api/user/%s", username))
                        .header(HttpHeaders.AUTHORIZATION, authorization))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void testGetUserNotFound() throws Exception {
        this.mockMvc
                .perform(get(String.format("/api/user/%s", wrongUsername))
                        .header(HttpHeaders.AUTHORIZATION, authorization))
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString("No user with id " + wrongUsername + " was found")));
    }
}
