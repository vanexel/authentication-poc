-- PW: oktaiscool
INSERT INTO USERS (ID, USERNAME, PASSWORD, BIRTH_DATE, FIRST_NAME, LAST_NAME) VALUES
('1', 'test1@test.com', '$2a$04$mgqGjc3P9S5eRegPuUaV9.YMyBGHpFjvPQSpNWUMU7CYmLDoh280a', '1978-04-06', 'blah 123', 'blah 456'),
('2', 'test2@test.com', '$2a$04$mgqGjc3P9S5eRegPuUaV9.YMyBGHpFjvPQSpNWUMU7CYmLDoh280a', '2000-09-17', 'blah', 'blah');