package org.stuff.example.poc.exception;

public class JwtTokenMissingException extends RuntimeException{
    public JwtTokenMissingException(String message) {
        super(message);
    }
}
