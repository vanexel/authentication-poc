package org.stuff.example.poc.config;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

@Component
public class JwtAccessDeniedHandler implements AccessDeniedHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtAccessDeniedHandler.class);
    @Override
    public void handle(HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse, AccessDeniedException accessDeniedException)
            throws IOException {

        LOGGER.info("Access denied without valid authorization ...");
        // This is invoked when user tries to access a secured REST resource without
        // the necessary authorization
        httpServletResponse.sendError(HttpServletResponse.SC_FORBIDDEN,
                accessDeniedException.getMessage());
    }
}
