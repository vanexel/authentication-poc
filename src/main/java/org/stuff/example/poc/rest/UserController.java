package org.stuff.example.poc.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.stuff.example.poc.model.dto.UserDTO;
import org.stuff.example.poc.service.UserService;

@RestController
@RequestMapping(path = {"api/user" })
@Api(value = "UserControllerAPI", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    UserService usersService;

    @ApiOperation(value = "Returns user details", hidden = false)
    @GetMapping(path = "/{userName}")
    public UserDTO getUser(@PathVariable("userName") String userName) {
        LOGGER.info("Retrieve information for user: {}", userName);
        return usersService.getUserByUserName(userName);
    }
}
