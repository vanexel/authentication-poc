package org.stuff.example.poc.rest;

import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.stuff.example.poc.model.dto.JwtDTO;
import org.stuff.example.poc.model.dto.LoginDTO;
import org.stuff.example.poc.service.AuthenticationService;

@RestController
@RequestMapping(value = "/auth")
public class AuthenticationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationController.class);
    public static final String AUTHORIZATION_HEADER = "Authorization";

    @Autowired
    AuthenticationService authenticationService;

    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Login for the authentication of a user.", hidden = false)
    public ResponseEntity<JwtDTO> authenticate(@RequestBody final LoginDTO loginDTO) {
        LOGGER.info("Authenticating user {}", loginDTO.getUserName());
        JwtDTO generatedJWT = authenticationService.authenticate(loginDTO);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer " + generatedJWT.getAuthorization());

        return new ResponseEntity<>(generatedJWT, httpHeaders, HttpStatus.OK);

    }
}
