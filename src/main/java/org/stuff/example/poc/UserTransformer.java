package org.stuff.example.poc;

import java.sql.Date;
import java.time.LocalDate;
import java.time.Period;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.stuff.example.poc.model.dto.UserDTO;
import org.stuff.example.poc.model.entity.UserEntity;

@Component
public class UserTransformer {
    public UserDTO entityToDTO(UserEntity userEntity) {
        if (userEntity == null)
            return null;
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(userEntity, userDTO);
        userDTO.setAge(getAge(userEntity.getBirthDate()));
        return userDTO;
    }

    public UserEntity DTOToEntity(UserDTO userDTO) {
        if (userDTO == null)
            return null;
        UserEntity userEntity = new UserEntity();
        BeanUtils.copyProperties(userDTO, userDTO);
        return userEntity;
    }

    private Integer getAge(Date birthDate) {
//        Date currentDate = new Date(System.currentTimeMillis());
        LocalDate today = LocalDate.now();
        LocalDate birthday = new java.sql.Date(birthDate.getTime()).toLocalDate();
        Period p = Period.between(birthday, today);
        return p.getYears();
    }
}
