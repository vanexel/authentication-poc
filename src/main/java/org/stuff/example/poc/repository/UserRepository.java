package org.stuff.example.poc.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.stuff.example.poc.model.entity.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, String>  {
    Optional<UserEntity> findByUsernameIgnoreCase(String username);
}
