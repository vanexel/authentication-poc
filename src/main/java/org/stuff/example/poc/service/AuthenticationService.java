package org.stuff.example.poc.service;

import org.springframework.http.ResponseEntity;
import org.stuff.example.poc.model.dto.JwtDTO;
import org.stuff.example.poc.model.dto.LoginDTO;

public interface AuthenticationService {

    JwtDTO authenticate(LoginDTO loginDTO);
}
