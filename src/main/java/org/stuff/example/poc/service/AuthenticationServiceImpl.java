package org.stuff.example.poc.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.stuff.example.poc.jwt.JwtTokenUtils;
import org.stuff.example.poc.model.dto.JwtDTO;
import org.stuff.example.poc.model.dto.LoginDTO;
import org.stuff.example.poc.model.entity.UserEntity;

@Service
public class AuthenticationServiceImpl implements AuthenticationService{
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationServiceImpl.class);

    @Autowired
    UserService userService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenUtils jwtTokenUtils;

    @Override
    public JwtDTO authenticate(LoginDTO loginDTO) {
        UserEntity userEntity = userService.getUserEntityByUserName(loginDTO.getUserName());
        if (checkPassword(loginDTO, userEntity)) {
            String token = jwtTokenUtils.generateAccessToken(userEntity);
            JwtDTO jwtDTO = new JwtDTO();
            jwtDTO.setAuthorization(token);
            return jwtDTO;
        }
        return null;
    }

    private boolean checkPassword(LoginDTO loginDTO, UserEntity userEntity) {
        if (passwordEncoder.matches(loginDTO.getPassword(), userEntity.getPassword())) {
            LOGGER.info("User successfully logged in: {}", userEntity.getUsername());
            return true;
        } else {
            LOGGER.error("Login failed for user: {}", userEntity.getUsername());
            throw new BadCredentialsException("Bad password");
        }
    }
}
