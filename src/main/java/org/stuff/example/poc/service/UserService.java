package org.stuff.example.poc.service;

import org.stuff.example.poc.model.dto.UserDTO;
import org.stuff.example.poc.model.entity.UserEntity;

public interface UserService {
    UserDTO getUserByUserName(String userName);

    UserEntity getUserEntityByUserName(String userName);
}
