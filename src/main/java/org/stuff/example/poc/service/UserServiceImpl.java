package org.stuff.example.poc.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stuff.example.poc.UserTransformer;
import org.stuff.example.poc.exception.UserNotFoundException;
import org.stuff.example.poc.model.dto.UserDTO;
import org.stuff.example.poc.model.entity.UserEntity;
import org.stuff.example.poc.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    UserRepository usersRepository;

    @Autowired
    UserTransformer userTransformer;

    @Override
    public UserDTO getUserByUserName(String userName) {
        return userTransformer.entityToDTO(getUserEntityByUserName(userName));
    }

    @Override
    public UserEntity getUserEntityByUserName(String userName) {
        return usersRepository.findByUsernameIgnoreCase(userName).orElseThrow(()->
                new UserNotFoundException(userName));
    }
}
