package org.stuff.example.poc.model.dto;

import java.io.Serializable;
import java.sql.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDTO implements Serializable {
    private Long id;
    private String username;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private Integer age;
}
