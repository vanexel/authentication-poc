package org.stuff.example.poc.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtDTO {
    private String authorization;
}
