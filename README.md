# Authentication Example

Project to demonstrate user authentication, Spring Security Configuration, JWT generation and JWT based stateless authentication

---

# Used Core Libraries

- [spring-boot](https://spring.io/projects/spring-boot)
- [spring-security](https://spring.io/projects/spring-security)
- [H2 database](https://www.h2database.com/)
- [Swagger](https://swagger.io/)

---

# Running the application

1. git clone https://vanexel@bitbucket.org/vanexel/authentication-poc.git
2. cd authentication-poc
3. mvn spring-boot:run

---

# Testing the application

1. Install Postman.
2. Send PORT request tu URL: http://localhost:8080/auth/login with body: {
  "userName": "test1@test.com",
  "password": "test"
}
3. Retrieve Authorization token from the response body.
4. Use the Authorization token to send a GET request to URL: http://localhost:8080/api/user/test1%40test.com; you should have in request an header with name Authorization and value: Bearer + the token from previous step.

---

## Application details

1. Application has Swagger configured, you can access it at address: http://localhost:8080/swagger-ui.html#/
2. Application has configured H2 in memory database; you can access the console at address http://localhost:8080/h2-console and login with sa/h2spring
3. Thee are two users defined, both with password "test"; you cand fins the sql used to insert the users in resources/data.sql
